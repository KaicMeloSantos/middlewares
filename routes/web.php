<?php

use App\Http\Middleware\PrimeiroMiddleware;
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/usuarios', "usuarioControlador@index")->middleware("primeiro");

Route::get('/terceiro', function () {
    return "Passou pelo terceiro middleware";
})->middleware("terceiro:Kaic,23");


Route::get('/produtos', "ProdutoControlador@index");

Route::get('/negado', function(){
    return "Acesso negado";
})->name('negado');

Route::post('/login', function(Request $req){

    $login_ok = false;

 switch($req->input('user')){


    case 'joao':
        $login_ok = $req->input('passwd') == "senhajoao";
    break;
    case "marcos":
        $login_ok = $req->input('passwd') ==="senhamarcos";
    break; 
    default:
        $login_ok = false;
 }

 if($login_ok){
     $login = ['user'-> $res->input('user')];
     $req->session()->put('login',$login);
    return response("login ok",200);
 }else{
    $req->session()->flush();
    return response("erro no login",404);
 }

});